import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        try {
            System.out.println("Input an integer whose factorial will be computed: ");
            int num = input.nextInt();

            int answer = 1;
            int counter = 1;

            if (num < 0) {
                System.out.println("Only positive numbers are accepted in this system");
            } else {
                for (counter = 1; counter <= num; counter++) {
                    answer *= counter;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            }
        }catch (Exception e){
            System.out.println("Only numbers are accepted in this system");
        }
    }
}
